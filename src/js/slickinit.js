
$('.slider').slick({
    autoplay:  true,
    autoplaySpeed: 2200,
    dots: true,
    arrows: false,
    responsive: [
        {
          breakpoint: 600,
          settings: {
            dots: false
          }
        }
    ]
});
$('.slider-feedback').slick({
    autoplay:  true,
    autoplaySpeed: 2200,
    centerMode: true,
    centerPadding: '0',
    dots: true,
    arrows: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
            breakpoint: 670,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false,
              centerPadding: '0',
            }
          }
    ]
});
   